﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tp2_Table
{
    public class Program
    {
        public static int Value(string Number_as_string)
        {
            int Number;
            if(int.TryParse(Number_as_string, out Number))
            { 
                return 1;
            }
            else
            {
                return 0;
            }
        }
        static void Main(string[] args)
        {
            int Number, lengthTable = 10, positionTable, NumberCheck = 0;
            int[] Table = new int[lengthTable];
            string Number_as_string;

            Console.WriteLine("Saisissez dix valeurs.");
            for (positionTable = 0; positionTable < lengthTable; positionTable++)
            {
                do
                {
                    Console.Write("{0}: ", positionTable + 1);
                    Number_as_string = Console.ReadLine();
                    int.TryParse(Number_as_string, out Number);
                    Table[positionTable] = Number;
                } while (Number_as_string.Length == 0);
            }
            Console.WriteLine("Saisissez une valeur: ");
            Number_as_string = Console.ReadLine();
            int.TryParse(Number_as_string, out Number);
            if (Number == Table[0])
            {
                NumberCheck = 1;
                Console.WriteLine("{0} est la 1-ere valeur saisie.", Number);
            }
            else
            {
                for (positionTable = 1; positionTable < lengthTable; positionTable++)
                {
                    if (Number == Table[positionTable])
                    {
                        NumberCheck = 1;
                        Console.WriteLine("{0} est la {1}-eme valeur saisie.", Number, positionTable + 1);
                    }
                }
            }
            if (NumberCheck == 0)
                {
                    Console.WriteLine("La valeur {0} n'est pas trouvable.", Number);
                }
            Console.ReadKey();
        }
    }
}
