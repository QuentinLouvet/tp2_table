﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using tp2_Table;

namespace tp2_Table_Tests
{
    [TestClass]
    public class TableTests
    {
        [TestMethod]
        public void Value()
        {
            Assert.AreEqual(0, Program.Value("hello"));
            Assert.AreEqual(1, Program.Value("132"));
        }

    }
}
